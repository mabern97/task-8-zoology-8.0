﻿using System;
namespace Task8Zoology
{
    public enum Gender
    {
        Female,
        Male
    }

    [Flags]
    public enum Skills
    {
        None = 1,
        Biter = 2,
        Climber = 4,
        Runner = 8,
    }

    public abstract class Animal
    {
        public string Name{ get; set; }
        public string Species { get; set; }
        public string Breed { get; set; }

        public int Age { get; set; }
        public Gender Gender { get; set; }

        // Interfaced variables
        /*public bool Runner { get; set; }
        public bool Climber { get; set; }
        public bool Biter { get; set; }*/
        public Skills Skills { get; set; }

        public Animal(string name, string species, string breed, int age)
        {
            Name = name;
            Species = species;
            Breed = breed;
            Age = age;
            Gender = Gender.Male;
            Skills = Skills.None;
        }

        public Animal(string name, string species, string breed, int age, Gender gender = Gender.Male)
        {
            Name = name;
            Species = species;
            Breed = breed;
            Age = age;
            Gender = gender;
            Skills = Skills.None;
        }

        public virtual void Kill(Animal animal)
        {
            Console.WriteLine($"{Name} found and killed a {animal.Species} named {animal.Name}. :(");
        }

        public virtual void Yawn()
        {
            Console.WriteLine($"{Species}::{Name} yawned. Maybe a bit tired ayy?");
        }

        public virtual void PrintDescription()
        {
            Console.WriteLine($"{Name} is a {Gender.ToString().ToLower()} {Species} who is {Age} years old.");
        }

        public void PrintSkills()
        {
            Console.WriteLine($"{Name} (A {Breed}) has the following skills: {Skills}");
        }
    }
}
