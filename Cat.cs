﻿using System;
namespace Task8Zoology
{
    public class Cat : Animal
    {
        public bool Striped { get; set; }

        public Cat(string name, int age, Gender gender, string breed = "Domesticated House Cat")
            :base(name, "Cat", breed, age, gender)
        {
            Striped = false;
        }

        public override void PrintDescription()
        {
            Console.WriteLine($"{Name} is a {Age} year old {(Striped == true ? "striped " : "")}{Species} (Breed: {Breed}). Rawr!");
        }

        public override void Kill(Animal animal)
        {
            Console.WriteLine($"{Name} pounced towards a {animal.Species} named {animal.Name}, dinner has been claimed!");
        }

        public Cat Crossbreed(Cat cat, string name)
        {
            string breed = $"{Species[0]}{cat.Species.Substring(1)}";

            Gender gender = this.Gender == Gender.Female && cat.Gender == Gender.Male ? Gender.Male : Gender.Female;

            Cat baby = new Cat(name, 1, gender);
            baby.Breed = $"{this.Gender} {this.Species} {gender} {cat.Species}";

            Console.WriteLine($"Created a {gender} {Species} {cat.Species} cross breed ({breed}), their name is '{name}'. How cute?");

            return baby;
        }
    }

    public class Tiger : Cat, IRunner, IClimber, IBiter
    {

        public Tiger(string name, int age, Gender gender, string breed)
            : base(name, age, gender, breed)
        {
            Species = "Tiger";

            Skills = Skills.Runner | Skills.Climber | Skills.Biter;
        }

        public void Run()
        {
            Console.WriteLine($"{Species}::Run: I am a named {Name} and I can run at a speed between 49-65 km/h!");
        }

        public void Climb()
        {
            Console.WriteLine($"{Species}::Climb: I am a named {Name} and I am running at a speed between 49-65 km/h!");
        }

        public void Bite()
        {
            Console.WriteLine($"{Species}::Climb: I am a named {Name} and if I bite you, i'll probably kill you.. Yeah :/");
        }
    }

    public class Lion : Cat, IRunner, IBiter
    {

        public Lion(string name, int age, Gender gender, string breed)
            : base(name, age, gender, breed)
        {
            Species = "Lion";

            Skills = Skills.Runner | Skills.Biter;
        }

        public void Run()
        {
            Console.WriteLine($"{Species}::Run: I am an apex predator named {Name} and I can run at a speed between 49-65 km/h!");
        }

        public void Bite()
        {
            Console.WriteLine($"{Species}::Climb: I am a named {Name} and once I bite you i'll just kill you, that's how hungry I am #apex");
        }
    }

    public class Jaguar : Cat, IRunner
    {

        public Jaguar(string name, int age, Gender gender, string breed)
            : base(name, age, gender, breed)
        {
            Species = "Jaguar";

            Skills = Skills.Runner;
        }

        public void Run()
        {
            Console.WriteLine($"{Species}::Run: I am an apex predator named {Name} and I can run at a speed between 49-65 km/h!");
        }
    }

    public class Liger : Tiger
    {
        public Liger(string name, int age, Gender gender)
            : base(name, age, gender, "Male Lion Female Tiger")
        {

        }
    }

    public class Tigon : Tiger
    {
        public Tigon(string name, int age, Gender gender)
            : base(name, age, gender, "Male Tiger Female Lion")
        {

        }
    }
}
