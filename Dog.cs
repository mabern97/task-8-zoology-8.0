﻿using System;
namespace Task8Zoology
{
    public class Dog : Animal
    {
        public Dog(string name, string breed, int age, Gender gender)
            : base(name, "Dog", breed, age, gender)
        {

        }

        /*public override void PrintDescription()
        {
            Console.WriteLine($"{Name} is a {(Striped == true ? "striped " : "")} {Species} that is {Age} years old. Rawr!");
        }*/

        public void Pounce(Animal animal)
        {
            Console.WriteLine($"{Name} pounced towards a {animal.Species} named {animal.Name}, dinner has been claimed!");
        }
    }

    public class Greyhound : Dog, IRunner
    {
        public Greyhound(string Name, int Age, Gender Gender)
            : base(Name, "Greyhound", Age, Gender)
        {
            Skills = Skills.Runner;
        }

        public void Run()
        {
            Console.WriteLine($"{Species}::Run: My name is {Name} and I am probably one of fastest runner dogs out there #kindaProud");
        }
    }

    public class GermanShepherd : Dog, IBiter, IRunner, IClimber
    {
        public GermanShepherd(string Name, int Age, Gender Gender)
            : base(Name, "German Shepherd", Age, Gender)
        {
            Skills |= Skills.Runner | Skills.Climber | Skills.Biter;
        }

        public void Run()
        {
            Console.WriteLine($"{Species}::Run: My name is {Name} and I can run pretty fast I think?");
        }

        public void Climb()
        {
            Console.WriteLine($"{Species}::Climb: My name is {Name} and I can jump fairly easily, probally will land on you though");
        }

        public void Bite()
        {
            Console.WriteLine($"{Species}::Bite: My name is {Name} and my bite force is pretty strong but i'll likely not kill you?");
        }
    }
}
