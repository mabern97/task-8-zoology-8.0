﻿using System;
using System.Collections.Generic;

namespace Task8Zoology
{
    class Program
    {
        static void Main(string[] args)
        {
            Cat stella = new Cat("Stella", 8, Gender.Female);
            Cat franky = new Cat("Franky", 4, Gender.Male);

            Dog whiskey = new Dog("Whiskey", "Dalmatian", 12, Gender.Male);
            Dog lucy = new Dog("Lucy", "Pinscher", 4, Gender.Female);
            Greyhound hugh = new Greyhound("Hugh", 10, Gender.Male);
            GermanShepherd bella = new GermanShepherd("Bella", 13, Gender.Female);

            Tiger vanessa = new Tiger("Vanessa", 10, Gender.Female, "Bengal");
            Lion grant = new Lion("Grant", 6, Gender.Male, "Southwest African");
            Liger jerry = new Liger("Jerry", 3, Gender.Male);
            Tigon anna = new Tigon("Anna", 7, Gender.Female);
            Jaguar coco = new Jaguar("Coco", 5, Gender.Female, "Panthera onca onca");

            // Crossbreeding
            Cat edward = vanessa.Crossbreed(grant, "Edward");
            Cat brandy = grant.Crossbreed(vanessa, "Brandy");
            franky.Kill(whiskey);

            List<Animal> pets = new List<Animal>()
            {
                stella,
                franky,
                whiskey,
                lucy,
                vanessa,
                grant,
                jerry,
                hugh,
                anna,
                coco,
                bella,
                edward,
                brandy,
            };

            Animal lastAnimal = null;

            Console.WriteLine($"\nPrinting out animal descriptions");
            foreach (Animal pet in pets)
            {
                pet.PrintDescription();
                pet.Yawn();

                if (lastAnimal != null)
                    lastAnimal.Kill(pet);

                lastAnimal = pet;
            }

            Console.WriteLine($"\nPrinting out animal skills");
            foreach (Animal pet in pets)
                pet.PrintSkills();

            Console.WriteLine("\nPrinting out all animals capable of running:");
            ListRunners(hugh, vanessa, grant);

            Console.WriteLine("\nPrinting out all animals capable of climbing:");
            ListClimbers(vanessa, jerry, anna);

            Console.WriteLine("\nPrinting out all animals capable of biting:");
            ListBiters();
        }

        static void ListRunners(params IRunner[] runners)
        {
            foreach (IRunner runner in runners)
                runner.Run();
        }

        static void ListClimbers(params IClimber[] climbers)
        {
            foreach (IClimber climber in climbers)
                climber.Climb();
        }

        static void ListBiters(params IBiter[] biters)
        {
            foreach (IBiter biter in biters)
                biter.Bite();
        }
    }
}
