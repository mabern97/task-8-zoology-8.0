﻿using System;
namespace Task8Zoology
{
    public interface IClimber
    {
        public void Climb();
    }
}
